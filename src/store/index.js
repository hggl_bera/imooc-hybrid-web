import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

/**
 * VueX作用：
 * 1、创建全局变量
 * 2、通过一些方法该改变全局变量的值
 */

/**
  * Store：
  *   vueX用到的核心概念均在此种
  *   在 vue 组件中，我们可以直接通过 this.$store = Store对象。
  *
  * State：
  *   vueX中声明的全局变量
  *
  * getter：
  *   相当于 vue 中的计算属性，用来监听计算
  *
  * mutation：
  *   vueX 中的操作数据的方法（vueX 中不推荐直接通过state为变量赋值）
  *   推荐使用 mutation 来修改，因为这样有迹可循（只能同步执行）
  *
  * action：
  *   帮助 mutation 进行异步操作
  *
  * module：
  *   将 store 分为多个模块， 每一个模块都是一个module
  *
  */
export default new Vuex.Store({
  state: {
    // 购物车数据
    shoppingDatas: []
  },
  mutations: {
    /**
     * 添加商品到购物车数据源
     */
    addShoppingData: function (state, goods) {
      // 判断购物车中是否包含该商品， 如果购物车中已经包含了该商品，那麽应该让该商品增加
      const isExist = state.shoppingDatas.some(item => {
        if (item.id === goods.id) {
          item.number += 1
          return true
        }
      })
      if (!isExist) {
        // 为商品新增属性
        // isCheck：表示商品是否选中
        // number： 表示商品的数量
        // 通过 Vue.set 的方法可以把新添加的属性变为 响应式的数据
        Vue.set(goods, 'isCheck', false)
        Vue.set(goods, 'number', 1)
        state.shoppingDatas.push(goods)
      }
    },
    /**
     * 更改指定商品数量
     */
    changeShoppingDataNumber: function (state, data) {
      /**
       * data: {
       * index: 指定商品
       * number：商品数量
       * }
       */
      state.shoppingDatas[data.index].number = data.number
    }
  },
  actions: {
  },
  modules: {
  }
})
